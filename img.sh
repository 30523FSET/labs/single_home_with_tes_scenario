i=1
echo "<tr>"
for f in results/profile_tes\=1.0*; do
  ff=$(echo $f | tr "_" " " | sed -r 's/results\/profile //')
  cat <<EOF
    <td>
      <figure>
        <a name='f$i'>
          <img width='326px' name='f$i' src='$f'/>
        </a>
        <small>$ff</small>
      </figure>
    </td>
EOF
  [[ $((i%3)) == 0 ]] && echo -e "</tr>\n<tr>"
  i=$((i+1))
done
