---
layout: default
permalink: /
---
<style>
  table {width: 100%}
  td, .div {text-align: center}
  figure {display: inline-block; margin: 0}
</style>
# A Case Study - Benefits of Residential Thermal Energy Storage under a Dynamic TOU Tariff

# Overview
Depending on location and demographic, heating and cooling can account for over 50% of consumer energy demand. Small scale distributed energy storage (ES), which includes electrical energy storage (EES) and thermal energy storage (TES) is predicted to be an important component in future energy systems. An intelligent energy management system that can coordinate the optimal inter-operation of various storage devices, distributed generation such as PV, to fulfill static forecasted or controlled (heat, cooling, electrical appliance) loads offers potential for significant economic gains.

TES (Thermal Energy Storage) in contrast to common chemical battery storage technologies has the advantage of being mechanically simple, having typically higher capacity, and should be cheaper to deploy. On the other hand TES can only (practically) serve thermal demands and typically has a worse efficiency factor and self-discharge characteristics than chemical batteries. This experiment investigates the potential economic gains from an intelligent energy management system with TES for a residential consumer facing a modern variable pricing retail tariff. The aim of the experiment is to determine a upper bound on the savings that can be achieved with TES in a residential setting, and the sensitivity of these saving to properties of the TES.

# Background
In Australia for the most part, and certainly at the residential level, the infrastructure for distribution of heat energy is not as well established as in Europe. However, there is still two clear *residential* examples of where TES can be successfully exploited. 1. Hot water heating systems. 2. Ice water storage systems (such as the [IceBear][1] systems). Since at a residential level heat distribution conduits are typical not available, heat and coolth demand are serviced from the electricity grid. In such a case TES serve as essentially a battery that can only be used for a given heating or cooling load. TES can act to smooth and shift that part of the electricity profile caused by thermal demand.

# Experiment
In this *day-ahead optimal scheduling* simulation experiment we model a single large residential household with a hot water system (HWES) and ~ice bucket energy storage (CWES) modeled after the [IceBear][1], subject to a modern variable TOU (Time of Use) tariff. To isolate the effects of the TES all load profiles except for the TES charge/discharge cycles are *fixed* and represent expectation values.

  - The only cooling load is space cooling via a HVAC.
  - The hot water demand is miscellaneous household uses, such as showering, a washing machine and miscellaneous hot water requirements. This is all grouped into one hot water demand profile that is shaped by these individual components.
  - The household is subject to an incline block DAP (Day Ahead Pricing) tariff with a demand charge.

## Site Description
<figure style="float:right; max-width:30%">
  <a name='f0'><img style="max-width:360px" src='img/simple-tes.png'/></a><br/>
  <small>Depiction of the site. All the loads (yellow) are lumped into one unscheduled load (there is no EV in this scenario). The red and blue boxes show the TES coupled to their respective thermal loads. Note there is no EES (green) in this scenario. A small PV installation (not shown) is also included in testing.</small>
</figure>
Unscheduled load profile was taken from average January weekday of a medium to large Melbourne house occupied by 5 students. The house has no dishwasher, no HVAC, and has a gas HWS. Without these components total daily consumption was low at 8.51kWh. For this experiment, HVAC and HWS energy load profiles were derived from other sources, bringing the total energy consumption to 24.3kWh which is closer to what one would expect on average for a 2-5 person home. Loads have the following breakdown:

  - 20% (4.86kWh); HWS load. Heating loads are all unscheduled and lumped together but could be decomposed to mainly misc+showers+washer.
  - 45% (10.94kWh); HVAC sub-network (*no* flexibility in cooling demand).
  - 35% (8.51kWh); Misc unscheduled electricity consumption component.

### TES Parameters
The most important numbers for TES are:

  - storage capacity.
  - maximum charge/discharge rates.
  - round trip efficiency factor (RTEF).
  - self discharge factor (SDF).

All of these numbers can vary depending on climate and changes in environment over the course of the day. However, in this study we assume they are all constant over the course a day. The HWES is modeled after a 125L system. The CWES is modeled after the ice-bear system.

    CWES:
      capacity: 70kWh
      max charge/discharge rates: +/- 7kW
      RTEF: 90%*
      SDF (per hour): 2.5%*

    HWES:
      capacity: 18kWh
      max charge/discharge rates: +/- 3kW
      RTEF: 90%
      SDF: 2.5%

Only limited information was available for the ice-bear. See [modeling notes](modeling-notes.md) for some background research. The efficiency factor and self discharge numbers are nominal values and uncertain. An objective of this report is to see how sensitive savings are to these numbers and we test with various values of efficiency factor and self-discharge in the experiments.

### Tariff
The retail tariffs is as follows.

    daily fixed: N/A (\\$/d)
    demand: 0.38 (\\$/kW/d)
    volume:
      off: {[0-6, 22-23], 0.19}
      shoulder: {[7-14, 20-21], 0.25}
      peak: {[15-19], 0.48}

The fixed daily charge is not relevant to this investigation. The TOU component is from an Australian residential TOU tariff. "[0-6]" means all times falling withing the 0th to 6th hours of the day *inclusive* starting midnight. The demand charge of $0.38/kw/d is typical of business tariff demand charges, although on the higher range. Note demand charges are not commonly seen on residential tariffs, but it is expected to be more common in the future.

### PV
We also add a relatively small PV installation. The max rate of the PV system is limited to 3kW.

## Methodology
The optimal operating profile for the HWES and CWES was calculated for the day-ahead under a number of different parameter settings:

    - TES: Present | Absent
      - TES round trip efficiency: 1.0, 0.96, 0.9
      - TES self discharge (per hour): 0.0, 0.04, 0.10
    - PV: Present | Absent

This results in 20 distinct cases (No TES or PV is present (1), PV is present but TES isn't (1), TES is present with the various RTEF and SDF while PV is or isn't present (9x2=18)). I used the same values on both TES (this limits the scope of the results, but not doing so leads to a very large number of combinations). The figures below show the exact load profiles for 10 cases.

## Results
The table below shows daily costs and total demand for all 20 cases. The figures below show the load and source profiles for all 20 cases.

#### Without PV
With perfect (RTEF=1.0, SDF=0.0) TES covering 65% (20% heating, 45% cooling) of energy demand the savings are \\$1.66/day (21%). With more realistic RETF of 96% and 90% these numbers are \\$1.34/day (17%) and \\$0.88/day (11%). In the worst case savings diminish to \\$0.36/day (4.5%). The self discharge had less of an effect than the RTEF. This is largely because we assumed the initial energy state of the device was zero and self discharge is proportional to energy state as a ratio of total energy capacity (this is inline with Newton's law of cooling).

#### With PV
Savings increase significantly with the addition on a relatively small PV. With only PV, savings are \\$3.74/day. Combining perfectly efficient TES and PV increases this to \\$6.37, i.e. by \\$2.63. Interestingly, this isn't sensitive to RTEF or SDF. The reason is PV energy is modeled as free, so RTEF and SDF losses don't cost anything. As long as the TES don't hit capacity constraints, the storage can charge/discharge the extra amount needed to account for the RTEF and SDF losses at no cost. Note further testing (not presented here) shows the savings are not very sensitive to PV sizing beyond about 3kW. Even at 20kW the savings only reduce \\$6.55. The reason is only 65% of the load is servicable via the TES.

#### Discussion and Future Work
Unless PV is also present, even in the perfectly efficient TES scenario, given the small volumes of the consumer, the capital expenditure required to install a IceBear like system is unlikely to payoff in a residential setting. With the story is different, and the results suggest the technologies should complement each other well even with a imperfectly performing TES.

This scenario supports the idea that the general properties we expect TES to have seem to complement EES quite well. In general, we expect TES has large capacity/\\$, large number of C/D cycles without much of a amortized degradation cost, while having with poor efficiencies. EES on the other hand has smaller capacity and higher amortized C/D costs, but better RTEF and much better SDF. Of course, TES can only service the thermal loads while EES can potentially service either. But if these are significant using TES to service thermal loads by arbitraging an excess of "free" PV energy, even at an efficiency loss, then saving the limited EES capacity for the higher priced times seems to make some sense. In this scenario at least, the addition of a small EES should bring retail purchased power to close to zero.

We presented results for one particular case study, on one particular day, and only touched on a few of the possible parameters. There are myriad different scenarios and myriad different parameters even before we start to consider what happens when we mix in demand response and other types of distributed energy generation. The sensitivity to tariff components would also interesting to analyze. The best approach to dealing with the overwhelming number of cases may be to make simulation and modeling tools available, extensive, and easy to use for the general public.


---

           PV  RTEF 1-SDF  Total Cost  Total Demand  Savings
        0   N     -     -      +7.983       +24.307   +0.000
        1   Y     -     -      +4.203       +11.794   +3.780
        2   N  1.00  1.00      +6.344       +24.307   +1.639
        3   N  1.00  0.96      +6.405       +24.679   +1.578
        4   N  1.00  0.90      +6.614       +24.393   +1.369
        5   N  0.96  1.00      +6.596       +25.372   +1.387
        6   N  0.96  0.96      +6.901       +26.572   +1.082
        7   N  0.96  0.90      +7.035       +26.076   +0.948
        8   N  0.90  1.00      +7.103       +26.938   +0.880
        9   N  0.90  0.96      +7.516       +27.795   +0.467
        10  N  0.90  0.90      +7.624       +27.094   +0.359
        11  Y  1.00  1.00      +1.570        +5.522   +6.413
        12  Y  1.00  0.96      +1.570        +5.522   +6.413
        13  Y  1.00  0.90      +1.570        +5.522   +6.413
        14  Y  0.96  1.00      +1.570        +5.522   +6.413
        15  Y  0.96  0.96      +1.570        +5.522   +6.413
        16  Y  0.96  0.90      +1.570        +5.522   +6.413
        17  Y  0.90  1.00      +1.570        +5.522   +6.413
        18  Y  0.90  0.96      +1.570        +5.522   +6.413
        19  Y  0.90  0.90      +1.570        +5.522   +6.413

---

{% include_relative table1.html %}

---

{% include_relative table2.html %}

---

{% include_relative table3.html %}

---



[1]: https://www.ice-energy.com/wp-content/uploads/2018/05/IB-20-ProductSheet-2017-US-D4.pdf
