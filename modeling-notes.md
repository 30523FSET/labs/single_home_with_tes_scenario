---
---
# Research & Analysis

## Ice Bear ##########
[Ice Bear Specs](https://www.ice-energy.com/wp-content/uploads/2018/05/IB-20-ProductSheet-2017-US-D4.pdf):

  - Rates: "On-peak demand reduction 7kW" equivalent peak power output. Claim requires +35A@240V > 7.5kW so as a rough first guesstimate rates [-7,7]
  - Energy: Claim 240000 BTU == 70.3kWh!
  - Efficiency: They don't say. Assume ~ 5%.
  - Thermal loss coeffs: Don't say assume pretty good. Around 5% is about right. Back of envelope calc gave me around %7/H for a domestic fridge from memory.

## Hot Water System ##########
There is two types of HWS. Instant (IHWS) and one with a tank (THWS). The THWS could be modeled crudely as a CDevice. But this ignores the fact the device is coupled to a predictable unscheduled demand profile, and ignores efficiency factor and thermal leakage. In this report we will consider the HWS as thermal storage coupled to an  instantaneous heat load that can be served either instantaneously (within the same scheduling interval) or from the reservior.

I'm assuming thermal leakage is fairly small since THWS can deal with only having supply available for 8H a day (minimum). Still it may be significant over the course of a whole day when considering what is needed to recharge. Try 2-5%. We still need a heat load profile.

**Some HWS Numbers** according to [reference][1]:

  - Single electric storage 125L hot water system.
  - 4kWh/day or 20% of total household load. Other sites ([8][8]) state ~20% energy consumption is to HW also.
  - The TES is expected to be able to charge fairly comfortably over 8H so will put max rate at 2kW.

This usage is broken down into showers, laundry, and misceallaneous other uses. Turns out all modern dishwashers use <15L per use, where as clothes washers use about 100L or more in some cases ([9][9]).

 > "Indoors, the shower is typically the biggest water user (34% of indoor water use in the average Australian home), followed by the toilet (26%) and laundry (23%)" -- [10][10].

Assume 50% of everything else (%17) is hotwater (9%). Breaking this down (np.round(np.array([34,23,9])*(100/66))) = array([52., 35., 14.]): 26%, 26% Showers morning night, 35% Clothes washer. 14% random.

## Tariff ##########
Tariff represents a realistic ~future residential scenario where block rates and demand charges are more common as in SME tariffs. It was pulled together data from three source to get a tariff featuring three components:

  - TOU Component from [Simply Enery plane][7] (summer weekday).
  - Block Rate: [Origin Energy][6] standing offer has two bands.
  - Demand Charge: [Table A.1 p39][5]

Residential tariffs currently rarely (AFAIK) feature block or demand components since the priceing is bundled and these components normally originate as network charges of the distributor. The TOU varying seasonally. We are only doing a single day simulation, and want to emphasize flex in HVAC. So choose summer weekday:

    fixed: 0.89 ($/d)
    demand: 0.28 ($/kW/d)
    volume:
      off: {[0-6, 22-33], 19.7}
      shoulder: {[7-14, 20-21], 25.35}
      peak: {[15-19], 47.40}
    blocks: {"<= 11kW": 1, "else": 1.10}

block scales the volume charge. In fact the source says block rate applies to the balance. Therefore price is a continuous but non smooth function of consumption.


## Australian Residential / SME Sub-Load Profiles ##########
[This site][9] lists some residential stats by load class. Annoyingly difficult to much better than this.

  - 23% to HWS.
  - 40% to heating and cooling but heating N/A in this scenario.
  - 37% everything else (unscheduled)

Since case study is for summer specificall "40%" may be on the lower side of things.

**Unscheduled**
Now we need daily data for summer weekday. Using my house (5 people mix of working and students) in January 2017 on weekdays:

    import numpy as np
    import pandas as pd
    from ami_data_parser import *
    f = 'data/sample-ami-meter-data-origin-20160401_20170401.csv'
    (df, meta) = parse(f)
    df = df[df.datetime.dt.month == 1
    df = df[df.datetime.dt.weekday <5]
    p = df.groupby(df.datetime.dt.hour).mean().values[:,0]

House has no HVAC but does have HWS. Could scale to account for HWS but house load is small as is so would want to scale it back up a bit anyway. So leave as is.

# Appendices

## The Controlled Load Case
It's not really a case of completely uncontrolled vs fully optimized. In Australia (and AFAIK, especially in SA) THWS are commonly connected to a "controlled load" / "dedicated cct" of the site meter, that is activated only during off peak hours. See [here][1]. The common controlled load method commonly exploited in Australia is very basic. There have been reports of expected off peak times becoming peaks due to the uncoordinated activation of off peak loads. In a micro-grid 1. direct coordination is more easily attainable. 2. more critical. What we want to do is basically implement the intelligent coordinated version of the "controlled load" dumb switching methodology. There is three cases of increasing complexity.

  1. Virtual IHWS. Will consider the operation of a THWS that is not connected to a dedicated cct the same as this.
  2. Dedicated cct.
  3. Fully optimized.

1 and 3 are easy. 1 is just the usual base case. 2 is like optimized but artificialy constrained to run only during some 8H block say 22:00 to 06:00.

### Open Questions

**Question:** Are controlled loads A. literally activated (thus controlled) by the distribution system / meter B. Only powered during the given time span C. Powered all the time but with a lower rate at the given time?

**Answer**: B. See below.

**Question:** When exactly are controlled load off peak times or ["night time"][2]?

**Answer:** It depends. Assuming all Aus setups work like [QLD][3]. QLD: Tariff 31: >= 8H per day at discretion on distributor. Tariff 33: >= 18H dido.

> "Electricity supply is made available for a minimum of eight hours per day at times set by the network owner (ENERGEX) at their discretion, generally between the hours of 10pm and 7am. On some days electricity supply may not be turned off at all. " -- [3][3].

> "Electricity supply is available for at least 18 hours per day and is switched via load control equipment supplied and maintained by ENERGEX. The times of supply are at ENERGEX's discretion. On some days electricity supply may not be turned off at all." -- [3][3]

[1]: https://www.canstarblue.com.au/electricity/controlled-load-tariff-can-save-money/
[2]: https://www.canstarblue.com.au/electricity/understanding-queensland-electricity-tariffs/
[3]: https://www.originenergy.com.au/terms-and-conditions/qld-electricity-tariffs.html
[4]: https://www.amazon.com/gp/product/B002635ODW/ref=as_li_tf_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B002635ODW&linkCode=as2&tag=bestprodtag114750-20
[5]: https://www.aer.gov.au/system/files/AER%20approved%20-%20CitiPower%20%20-%202018%20Pricing%20Proposal%20-%2012%20October%202017.pdf
[6]: https://www.originenergy.com.au/content/dam/origin/residential/docs/energy-price-fact-sheets/vic/1Jan2018/VIC_Electricity_Residential_AusNet%20Services_Standard%20Published%20Rate.PDF
[7]: https://gitlab.com/electricity-cost-analysis-tool/docs/blob/master/sample-tariffs/simplyenergy-simply-plus-35.html
[8]: http://www.yourhome.gov.au/energy/heating-and-cooling
[9]: https://www.harveynorman.com.au/
[10]: https://www.eea.europa.eu/data-and-maps/indicators/household-energy-consumption/household-water-consumption
