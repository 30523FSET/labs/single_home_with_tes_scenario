import sys
import re
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from powermarket.network import *
from powermarket.device import *
from powermarket.writer import *
from powermarket.analytics.templates import *
from powermarket.analytics.matplotlibwriter import *


reader = NetworkReader('single_home_tes.py-s=1.0')
(first, last) = (reader.first(), reader.last())

# Get a supply device for future use. Supply device is immutable
supply = dict(last.device.leaf_devices())['n.home.supply']

df0 = pd.DataFrame(dict(reader.first().leaf_items()))
reader = NetworkReader('single_home_tes.py-s=1.0')
df1 = pd.DataFrame(dict(reader.last().leaf_items()))
reader = NetworkReader('single_home_tes.py-s=0.98')
df2 = pd.DataFrame(dict(reader.last().leaf_items()))
reader = NetworkReader('single_home_tes.py-s=0.95')
df3 = pd.DataFrame(dict(reader.last().leaf_items()))

l0 = df0.sum(axis=1).values
l1 = -1*df1['n.home.supply'].values
l2 = -1*df2['n.home.supply'].values
l3 = -1*df3['n.home.supply'].values
print('|peak|total demand (kWh)|cost($)|')
for l in [l0, l1, l2, l3]:
  print('%.3f| %.3f| %.3f' % (l.sum(), l.max(), -1*supply.u(-1*l, 0)))
