#!/usr/bin/env python3
import itertools
import numpy as np
import pandas as pd
from collections import OrderedDict as odict
import matplotlib.pyplot as plt
import device_kit
from scenario import make_deviceset
import logging
from logging import debug, info
logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)


_cases = [
  [False, False, 1.0, 1.0],
  [False, True, 1.0, 1.0],
] + list(itertools.product([True], [False, True], [1.0, 0.96, 0.9], [1.0, 0.96, 0.9]))


def cases():
  for c in _cases:
    yield odict(zip(('tes', 'pv', 'rtef', 'sf'), c))


def tag_case(c):
  _str = 'tes=%s_pv=%s' % ('Y' if c['tes'] else 'N', 'Y' if c['pv'] else 'N')
  _str += '_rtef=%.2f_sf_%.2f' % (c['rtef'], c['sf']) if c['tes'] else ''
  return _str


def main():
  pd.set_option('display.float_format', lambda v: '%+0.3f' % (v,),)
  results = pd.DataFrame({} , columns=('PV', 'RTEF', '1-SDF', 'Total Cost', 'Total Demand'))
  for s in cases():
    debug('-'*100)
    (tes, pv, rtef, sf) = list(s.values())
    name = tag_case(s)
    info(name)
    efficiency=1-(1-rtef)/2
    model = make_deviceset(tes, tes, efficiency, efficiency, sf, sf, pv)

    # Solve
    (x, solve_meta) = device_kit.solve(model, p=0, solver_options={'ftol': 1e-5})
    debug(solve_meta.message)

    # Table results
    supply = model.get('supply')
    df = pd.DataFrame.from_dict(dict(model.map(x)), orient='index')
    cost = -supply.u(df.loc['home.supply'].values, 0)
    total = -df.loc['home.supply'].values.sum()
    result = (
      'Y' if pv else 'N',
      '%.2f' % rtef if tes else '-',
      '%.2f' % sf if tes else '-',
      cost,
      total
    )
    info('|%s|%s|%s|%.2f|%.2f|' % result)
    results.loc[len(results)] = result
    plot(df, name)

  # Print table
  results['Savings'] = results['Total Cost'][0] - results['Total Cost']
  print(results)


def plot(df, name):
  plt.cla()
  df = df.transpose().sort_index(axis=1)
  colors = {'pv': 'lime'}
  if 'home.hvac.cwes' in df.columns:
    df['home.hvac'] = df['home.hvac.hvac'] + df['home.hvac.cwes']
    del df['home.hvac.hvac'], df['home.hvac.cwes']
  if 'home.hotwater.hwes' in df.columns:
    df['home.hotwater'] = df['home.hotwater.hotwater'] + df['home.hotwater.hwes']
    del df['home.hotwater.hotwater'], df['home.hotwater.hwes']
  for k,v in df.items():
    k = k.split('.')[-1]
    plt.step(np.arange(24), v, label=k, color=colors[k] if k in colors else None)
    plt.legend(
      prop={'size': 11},
      loc='upper right',
      framealpha=0.6,
      frameon=True,
      fancybox=True,
      borderaxespad=-3
    )
    plt.ylim(-3.1,2.1)
    plt.grid(True)
  plt.savefig('results/profile_%s.png' % name)


if __name__ == '__main__':
  main()
