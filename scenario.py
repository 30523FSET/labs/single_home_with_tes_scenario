'''
Single home with a TOU tarriff. The network is essentially the user's home. There is only one objective
- maximize the user's pay off. The user has a "Retail" device which represents their connection to the
retailer. It can be considered like a DG owned by the user with a funny cost function.
'''
import sys
import numpy as np
from numpy import array, ones, zeros, stack, hstack, vstack, linspace, sin, pi
import numdifftools as nd
from device_kit import *


meta = {
  'title': ""
}


def make_deviceset(
    cwes=False, hwes=False,
    cwes_efficiency=0.95, cwes_sustainment=0.975,
    hwes_efficienct=0.95, hwes_sustainment=0.975,
    pv=False, ees=False
  ):
  cwes_params = {
    'bounds': (-3,3),
    'c1': 0.0,
    'reserve': 0.0,
    'capacity': 18,
    'efficiency': cwes_efficiency,
    'sustainment': cwes_sustainment,
  }
  hwes_params = {
    'bounds': (-7,7),
    'c1': 0.0,
    'reserve': 0.0,
    'capacity': 70,
    'efficiency': hwes_efficienct,
    'sustainment': hwes_sustainment,
  }
  loads = make_loads()
  devices = [
    loads['unsched'],
    ADevice('supply', 24, [-50, 0], f=Retail())
  ]
  if cwes:
    devices += [DeviceSet('hvac', [loads['hvac'], SDevice('cwes', 24, **cwes_params)], sbounds=(0,100))]
  else:
    devices += [loads['hvac']]
  if hwes:
    devices += [DeviceSet('hotwater', [loads['hotwater'], SDevice('hwes', 24, **hwes_params)], sbounds=(0,100))]
  else:
    devices += [loads['hotwater']]
  if pv:
    devices += [make_pv()]
  if ees:
    devices += [make_ees()]
  return DeviceSet('home', devices, sbounds=(0,0))


def make_loads():
  ''' Return static loads. '''
  unsched_demand = array([0.457, 0.462, 0.412, 0.313, 0.304, 0.301, 0.221, 0.171, 0.160, 0.200, 0.222, 0.251, 0.342, 0.425, 0.388, 0.368, 0.377, 0.479, 0.445, 0.462, 0.456, 0.385, 0.434, 0.474])
  heat_demand = array([0.027, 0.000, 0.000, 0.000, 0.017, 0.102, 0.385, 0.673, 0.407, 0.119, 0.015, 0.000, 0.000, 0.000, 0.000, 0.002, 0.012, 0.097, 0.395, 0.715, 0.586, 0.621, 0.479, 0.208])
  hvac_demand = array([0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.200, 0.205, 0.409, 0.716, 1.022, 1.125, 1.329, 1.023, 0.920, 0.818, 0.716, 0.614, 0.511, 0.511, 0.411, 0.306, 0.102])
  return {
    'hvac': Device('hvac', 24, (hvac_demand,)),
    'hotwater': Device('hotwater', 24, (heat_demand,)),
    'unsched': Device('unsched', 24, (unsched_demand,)),
  }


def make_pv(max_rate=3, area=5, efficiency=0.9):
  ''' Return a PV device. '''
  solar_intensity = np.maximum(0, np.sin(np.linspace(-np.pi/2, np.pi*2-np.pi/2, 24)))
  lbounds = -1*np.minimum(max_rate, solar_intensity*efficiency*area)
  return PVDevice('pv', 24, (lbounds, 0))


def make_ees(max_rate=1.8, capacity=5):
  ''' No diff for battery '''
  return SDevice('ees', 24, [-max_rate, max_rate],
    damage_depth=0.1,
    reserve=0.5,
    capacity=capacity,
    efficiency=0.98
  )


class Retail():
  ''' Callable cost function suitable for ADevice.f:

    fixed: N/A ($/d)
    demand: 0.28 ($/kW/d)
    volume:
      off: {[0-6, 22-23], 19.7}
      shoulder: {[7-14, 20-21], 25.4}
      peak: {[15-19], 47.40}
    blocks: {"<= XkW": 1, "else": Y}

  With the block rate volume charge is a continuous but not c.d increasing p.w. linear function.
  The demand charge component is also continuous but not c.d.

  __call__(), deriv(), and hess() all expect to be called with a -ve number.
  '''
  tou_price = np.hstack((np.ones(7)*0.19, np.ones(8)*0.25, np.ones(5)*0.48, np.ones(2)*0.254, np.ones(2)*0.157))
  demand_price = 0.38
  block_factor = 1.0
  block_start = 10

  def __init__(self):
    pass

  def __call__(self, r):
    ''' Return the utility (-ve the cost) of supplying r. '''
    return -self.cost(-r)

  def deriv(self):
    return lambda r: self._deriv(-r)

  def _deriv(self, r):
    ''' np.choose is a confusing function. np.choose(r < 2, ['N', 'Y']) '''
    volume_deriv = np.choose(r < self.block_start, [self.tou_price*self.block_factor, self.tou_price])
    demand_deriv = np.choose(r < np.max(r), [self.demand_price, 0])
    return volume_deriv + demand_deriv

  def hess(self):
    return nd.Hessian(lambda r: np.zeros((len(r), len(r))))

  def cost(self, r):
    ''' r is expected to be a +ve number '''
    return self.costs_volume(r).sum() + self.costs_demand(r).sum()

  def costs_demand(self, r):
    ''' Pretend demand charges are per interval so we can show how usage effects costs. '''
    return np.choose(r < np.max(r), [self.demand_price, 0])*r

  def costs_volume(self, r):
    return np.minimum(r, self.block_start)*self.tou_price + np.maximum(r - self.block_start, 0)*self.tou_price*self.block_factor

  def to_dict(self):
    return {}
